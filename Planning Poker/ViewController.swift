//
//  ViewController.swift
//  Planning Poker
//
//  Created by Marcin Jackowski on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationItem.title = "PLANNING POKER"
    }
}