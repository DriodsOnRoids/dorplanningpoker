//
//  VotingViewController.swift
//  Planning Poker
//
//  Created by Paweł Sternik on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit

class VotingViewController: UIViewController {

// MARK: View Controller lifecycle methods
    
    var chosenCardNumber: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
// MARK: Actions
    
    @IBAction func confirmationButtonAction(sender: ConfirmationButton) {
        switch sender.buttonState {
        case .Show:
            ()
        case .Next:
            ()
        default:
            break
        }
    }
}
