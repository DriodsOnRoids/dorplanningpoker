//
//  UserTableViewCell.swift
//  Planning Poker
//
//  Created by Marcin Jackowski on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
