//
//  ConnectionManager.swift
//  Planning Poker
//
//  Created by Pawel Sternik on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import Foundation
import MultipeerConnectivity

struct ConnectionManagerSingleton {
    
    static var instance: ConnectionManager?
    
}

protocol ConnectionManagerDelegate {
    
    func didUserConnectionUpdated(users: [String])
    func didStartVotingSession()
    func didStartVoting(votingName: String)
    func didFinishVoting(votingName: String)
    func didReciveVotes(votes: [String: AnyObject?])
    
}

enum UserType {
    
    case ProjectManager
    case Developer
    
}

class ConnectionManager: NSObject {
    
    private let serviceType = "DOR-Poker"
    
    var delegate: ConnectionManagerDelegate?
    
    private var session: MCSession!
    private var peerID: MCPeerID!
    private var advertiser: MCAdvertiserAssistant!
    private var internalBrowser: MCBrowserViewController?
    private(set) var userRole: UserType
    private(set) var userName: String
    
    /**
     Execute it only once with proper `nickname`. Then use `ConnectionManagerSingleton.instance`.
     - parameter nickname: The username.
     - returns: self
     */
    init(nickname: String, type: UserType) {
        userRole = type
        userName = nickname
        super.init()
        peerID = MCPeerID(displayName: nickname)
        session = MCSession(peer: peerID)
        session.delegate = self
        advertiser =  MCAdvertiserAssistant(serviceType: serviceType, discoveryInfo: nil, session: session)
        advertiser.delegate = self
        ConnectionManagerSingleton.instance = self
    }
    
    func browser() -> MCBrowserViewController {
        internalBrowser = MCBrowserViewController(serviceType: serviceType, session: session)
        internalBrowser!.delegate = self
        
        return internalBrowser!
    }
    
    func closeBrowser() {
        internalBrowser?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func startAdvertising() {
        if userRole == .ProjectManager {
            advertiser.start()
        }
    }
    
    func stopAdvertising() {
        advertiser.stop()
    }
    
    func connectedUsers() -> [String] {
        var users = [String]()
        session.connectedPeers.forEach {
            users.append($0.displayName)
        }
        
        return users
    }
    
    func startVoting(votingName: String) {
        let inputDictionary = [
            "type" : "start_voting",
            "voting_name" : votingName
        ]
        sendDictionaryToAllUsers(inputDictionary)
    }
    
    func sendVote(value: Int) {
        let inputDictionary = [
            "type" : "send_vote",
            "nickname" : userName,
            "value" : value
        ] as [String: AnyObject]
        
        sendDictionaryToAllUsers(inputDictionary)
    }
    
    private func sendDictionaryToAllUsers(inputDictionary: [String: AnyObject]) {
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(inputDictionary, options: .PrettyPrinted)
             try session.sendData(data, toPeers: session.connectedPeers, withMode: .Reliable)
        } catch {
            
        }
    }
    
    deinit {
        advertiser.stop()
    }
    
}

extension ConnectionManager: MCSessionDelegate {
    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        print(state.rawValue)
        if state != MCSessionState.Connecting {
            let users = connectedUsers()
            delegate?.didUserConnectionUpdated(users)
        }
    }
    
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        let string = String(data: data, encoding: NSUTF8StringEncoding)
        print("\(string) from \(peerID.displayName)")
        print(session.connectedPeers)
        
        let jsonObject = try! NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as! NSDictionary
        
        guard let type = jsonObject["type"] as? String else {
            return
        }
        
        guard let delegateUnwrapped = delegate else {
            return
        }
        
        switch type {
        case "start_session":
            delegateUnwrapped.didStartVotingSession()
        case "start_voting":
            guard let votingName = jsonObject["voting_name"] else { return }
            delegateUnwrapped.didStartVoting(String(votingName))
        default:
            return
        }
    }

    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        print("\(stream) from \(peerID.displayName)")
    }
    
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        print("didStartReceivingResourceWithName")
    }
    
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        print("didFinishReceivingResourceWithName")
    }
    
}

extension ConnectionManager: MCBrowserViewControllerDelegate {
    
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
    }
    
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
        internalBrowser?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func browserViewController(browserViewController: MCBrowserViewController, shouldPresentNearbyPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) -> Bool {
        return true
    }
    
}

extension ConnectionManager: MCAdvertiserAssistantDelegate {
    
    func advertiserAssistantWillPresentInvitation(advertiserAssistant: MCAdvertiserAssistant) {
        print("advertiserAssistantWillPresentInvitation")
    }
    
    func advertiserAssistantDidDismissInvitation(advertiserAssistant: MCAdvertiserAssistant) {
        print("advertiserAssistantDidDismissInvitation")
    }
    
}