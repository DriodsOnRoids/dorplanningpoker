//
//  CardPickerViewController.swift
//  Planning Poker
//
//  Created by Michal Pyrka on 04/02/16.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit
import ARNRouletteWheelView

class CardPickerViewController: UIViewController {
    @IBOutlet weak var chosenCard: UIView!

    @IBOutlet weak var chosenLabel: UILabel!
    var reversed = false
    
    @IBOutlet weak var cardCollectionView: CardCollectionView! {
        didSet {
            cardCollectionView.collectionViewLayout = cardCollectionView.rouletteLayout()
        }
    }
    @IBOutlet weak var bottomCardPickerConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardCollectionView.dataSource = self
        cardCollectionView.delegate = self
        chosenCard.layer.borderColor = UIColor.blackColor().CGColor
        chosenCard.layer.borderWidth = 2.0
        chosenCard.layer.cornerRadius = 10.0

        [.Normal, .Selected, .Disabled, .Highlighted].forEach {
            self.nextButton.setTitleColor(UIColor.whiteColor(), forState: $0)
        }
        
        nextButton.alpha = 0.0
    }

    
    func hideCards() {
        self.bottomCardPickerConstraint.constant = -120.0
        UIView.animateWithDuration(
            0.3,
            delay: 0.0,
            usingSpringWithDamping: 0.5,
            initialSpringVelocity: 0.2,
            options: UIViewAnimationOptions.AllowUserInteraction,
            animations: { () -> Void in
                self.cardCollectionView.layoutIfNeeded()
            },
            completion: nil)
    }
    
    func animateReverse() {
        if reversed {
            UIView.transitionWithView(chosenCard, duration: 0.5, options: .TransitionFlipFromRight, animations: nil, completion: nil)
            reversed = true
        } else {
            UIView.transitionWithView(chosenCard, duration: 0.5, options: .TransitionFlipFromLeft, animations: nil, completion: nil)
            reversed = false
        }
    } 
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toVotingViewController" {
            let cardValue = Int(chosenLabel.text!)!
            
            let viewController = segue.destinationViewController as? VotingViewController
            viewController?.chosenCardNumber = cardValue
            
            ConnectionManagerSingleton.instance?.sendVote(cardValue)
        }
    }
    
}

extension CardPickerViewController: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        UIView.animateWithDuration(0.3) { () -> Void in
            self.nextButton.alpha = 1.0
        }
        let cardCell = collectionView.cellForItemAtIndexPath(indexPath) as! CardCell
        chosenLabel.text = cardCell.cardView!.numberLabel.text
        animateReverse()
    }
    
}

extension CardPickerViewController: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return CardDifficulty.countCases()
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cardCell = collectionView.dequeueReusableCellWithReuseIdentifier(String(CardCell), forIndexPath: indexPath) as! CardCell
        let cardView = CardView(frame: cardCell.bounds)
        cardCell.addSubview(cardView)
        cardCell.cardView = cardView
        cardCell.cardView!.numberLabel.text = CardDifficulty(rawValue: indexPath.row)!.chosenNumber
        return cardCell
    }

}



protocol CaseCountable {
    static func countCases() -> Int
    static var caseCount: Int { get }
}

extension CaseCountable where Self : RawRepresentable, Self.RawValue == Int {
    static func countCases() -> Int {
        var count = 0
        while let _ = Self(rawValue: count) {
            count++
        }
        return count
    }
}



enum CardDifficulty: Int, CaseCountable {
    static var caseCount = CardDifficulty.countCases()

    case Zero = 0, One, Two, Three, Five, Eight, Thirteen
    
    var chosenNumber: String {
        switch self {
        case .Zero: return "0"
        case .One: return "1"
        case .Two: return "2"
        case .Three: return "3"
        case .Five: return "5"
        case .Eight: return "8"
        case .Thirteen: return "13"
        }
    }
}

extension UIViewController {
    
    func delay(delay: Double, closure: () -> ()) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue(), closure)
    }

}
