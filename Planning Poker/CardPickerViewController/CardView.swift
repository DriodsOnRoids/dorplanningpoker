//
//  CardView.swift
//  Planning Poker
//
//  Created by Michal Pyrka on 04/02/16.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit

class CardView: UIView {
        
    let numberLabel: UILabel
    override init(frame: CGRect) {
        numberLabel = UILabel(frame: frame)
        numberLabel.text = "?"
        numberLabel.font = UIFont.systemFontOfSize(36.0)
        numberLabel.textAlignment = .Center
        super.init(frame: frame)
        addSubview(numberLabel)
        backgroundColor = .whiteColor()
        layer.borderColor = UIColor.blackColor().CGColor
        layer.borderWidth = 2.0
        layer.cornerRadius = 10.0
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
