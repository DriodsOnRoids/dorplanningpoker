//
//  CardCell.swift
//  Planning Poker
//
//  Created by Michal Pyrka on 04/02/16.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit
import ARNRouletteWheelView

class CardCell: ARNRouletteWheelCell {
    
    var cardView: CardView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10.0
//        layer.shouldRasterize = true
    }
    
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        guard let layoutAttributes = layoutAttributes as? ARNRouletteWheelLayoutAttributes else {
            return
        }
        self.layer.anchorPoint = layoutAttributes.anchorPoint
        self.center = CGPoint(x: self.center.x, y: self.center.y + ((layoutAttributes.anchorPoint.y - 0.5) * CGRectGetHeight(self.bounds)));
    }
    
}