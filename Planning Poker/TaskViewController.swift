//
//  TaskViewController.swift
//  Planning Poker
//
//  Created by Marcin Jackowski on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import Foundation
import UIKit

class TaskViewController: ViewController {
    
    @IBOutlet weak var taskTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func okActionButton(sender: UIButton) {
        if let taskName = taskTextField.text where taskName.characters.count > 0 {
            ConnectionManagerSingleton.instance?.startVoting(taskName)
            performSegueWithIdentifier("projectManagerShowVoting", sender: self)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        view.endEditing(true)
    }
}