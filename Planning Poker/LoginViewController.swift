//
//  LoginViewController.swift
//  Planning Poker
//
//  Created by Marcin Jackowski on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class LoginViewController: ViewController {
    
    var sessionTitle: String?
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var managerSwitch: UISwitch!
    
    @IBAction func okButtonAction(sender: UIButton) {
        guard let nickname = usernameTextField.text where nickname.characters.count > 0 else { return }
        
        let userType = managerSwitch.on ? UserType.ProjectManager : UserType.Developer
        let connectionManager = ConnectionManager(nickname: nickname, type:userType)
        if managerSwitch.on {
            connectionManager.startAdvertising()
            performSegueWithIdentifier("ShowUsers", sender: self)
        } else {
            connectionManager.delegate = self
            presentViewController(connectionManager.browser(), animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        view.endEditing(true)
    }
}

extension LoginViewController: ConnectionManagerDelegate {
    
    func didStartVoting(votingName: String) {
        sessionTitle = votingName
        
        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
            ConnectionManagerSingleton.instance?.delegate = nil
            ConnectionManagerSingleton.instance?.closeBrowser()
            self.performSegueWithIdentifier("toPickingCardViewController", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toPickingCardViewController" {
            segue.destinationViewController.title = sessionTitle
        }
    }
    
    func didUserConnectionUpdated(users: [String]) { }
    func didFinishVoting(votingName: String) { }
    func didReciveVotes(votes: [String: AnyObject?]){ }
    func didStartVotingSession() {}
    
}