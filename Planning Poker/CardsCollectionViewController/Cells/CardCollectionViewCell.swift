//
//  CardCollectionViewCell.swift
//  Planning Poker
//
//  Created by Paweł Sternik on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

// Frameworks
import UIKit
import ChameleonFramework

enum CardCollectionViewCellState {
    case Voted
    case Unknown
    case Estimated(Int)
}

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    
    var state: CardCollectionViewCellState = .Unknown {
        didSet {
            let defaultSetupCell: (text: String) -> () = { [unowned self] text in
                self.backgroundColor = .whiteColor()
                self.titleLabel.textColor = .blackColor()
                self.usernameLabel.textColor = .blackColor()
                self.titleLabel.text = text
            }
            
            switch state {
            case .Voted:
                titleLabel.textColor = .whiteColor()
                UIView.animateWithDuration(0.3,
                animations: {
                    self.titleLabel.alpha = 0.0
                },
                completion: { finished in
                    UIView.animateWithDuration(0.3,
                    animations: {
                        self.backgroundColor = .flatGreenColor()
                        self.usernameLabel.textColor = .whiteColor()
                        self.checkmarkImageView.alpha = 1.0
                    })
                })
            case .Estimated(let estimation):
                defaultSetupCell(text: "\(estimation)")
            case .Unknown:
                defaultSetupCell(text: "?")
            }
        }
    }
    
// MARK: Initial methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        awakeFromNibSetup()
    }
    
// MARK: Setup
    
    func awakeFromNibSetup() {
        if let image = UIImage(named: "CheckmarkLogo") {
            checkmarkImageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
            checkmarkImageView.tintColor = .whiteColor()
        }
        checkmarkImageView.alpha = 0.0
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.blackColor().CGColor
        layer.cornerRadius = 12.0
    }
    
}
