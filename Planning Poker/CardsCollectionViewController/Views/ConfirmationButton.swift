//
//  ConfirmationButton.swift
//  Planning Poker
//
//  Created by Paweł Sternik on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit

enum ConfirmationButtonState {
    case Next
    case Show
    case DisabledShow
}

@IBDesignable
class ConfirmationButton: UIButton {
   
    @IBInspectable
    var buttonState: ConfirmationButtonState = .DisabledShow {
        didSet {
            let defaultSetup: (text: String) -> () = { [unowned self] text in
                self.enabled = true
                self.backgroundColor = .flatGreenColor()
                self.setTitleColor(.whiteColor(), forState: .Normal)
                self.setTitle(text, forState: .Normal)
            }
            
            switch buttonState {
            case .Next:
                defaultSetup(text: "Next")
            case .Show:()
                defaultSetup(text: "Show")
            case .DisabledShow:()
                self.backgroundColor = .flatGrayColor()
                self.enabled = false
                self.alpha = 0.5
                self.setTitleColor(.whiteColor(), forState: .Normal)
                self.setTitle("Show", forState: .Normal)
            }
        }
    }
    
// MARK: Initial methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
// MARK: Setup
    
    func initialSetup() {
        buttonState = .DisabledShow
    }

}
