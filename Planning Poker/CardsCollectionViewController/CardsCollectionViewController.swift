//
//  CardsCollectionViewController.swift
//  Planning Poker
//
//  Created by Paweł Sternik on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

// Frameworks
import UIKit
import KTCenterFlowLayout

class CardsCollectionViewController: UICollectionViewController {

    var cardsArray = [[String: AnyObject?]]()
    private let standardMargin: CGFloat = 8.0
    let centerLayout = KTCenterFlowLayout()
    
// MARK: View Controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDidLoadSetup()
    }
    
// MARK: Setup
    
    func viewDidLoadSetup() {
        centerLayout.minimumLineSpacing = standardMargin
        centerLayout.minimumInteritemSpacing = standardMargin
        collectionView?.collectionViewLayout = centerLayout
    
        cardsArray = []
        ConnectionManagerSingleton.instance?.connectedUsers().forEach {
            cardsArray.append(["estimation": nil, "username": $0])
        }
    }
    
// MARK: Collection View Delegate and DataSource
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardsArray.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CardCell", forIndexPath: indexPath) as? CardCollectionViewCell
        if let finished = cardsArray[indexPath.row]["voted"] as? Bool where finished == true {
            cell?.state = .Voted
        } else if let estimation = cardsArray[indexPath.row]["estimation"] as? Int {
            cell?.state = .Estimated(estimation)
        } else {
            cell?.state = .Unknown
        }
        
        if let username = cardsArray[indexPath.row]["username"] as? String {
            cell?.usernameLabel.text = username
        }
        
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let collectionViewDimension = collectionView.frame.size
        var widthCell: CGFloat = 0.0
        
        switch cardsArray.count {
        case 1, 2:
            widthCell = (collectionViewDimension.width - standardMargin) / 2.0
            let insetTop = (collectionViewDimension.height - (widthCell * 1.7)) / 2.0
            collectionView.contentInset = UIEdgeInsets(top: insetTop, left: 0.0, bottom: 0.0, right: 0.0)
        case 3, 4:
            widthCell = collectionViewDimension.width / 3.0
        default:
            widthCell = 0.0
        }
        
        return CGSize(width: widthCell, height: widthCell * 1.7)
    }
}
