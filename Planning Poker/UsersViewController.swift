//
//  UsersViewController.swift
//  Planning Poker
//
//  Created by Marcin Jackowski on 04.02.2016.
//  Copyright © 2016 Droids on Roids. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class UsersViewController: ViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var dataSet = [String]()
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ConnectionManagerSingleton.instance?.delegate = self
        tableView.registerNib(UINib(nibName: "UserTableViewCell", bundle: nil),
            forCellReuseIdentifier: "UserTableViewCell")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        ConnectionManagerSingleton.instance?.stopAdvertising()
    }
    
    // MARK: Outlets actions
    
    @IBAction func startButtonAction(sender: UIBarButtonItem) {
        ConnectionManagerSingleton.instance?.stopAdvertising()
        performSegueWithIdentifier("ShowTask", sender: sender)
    }
    
}

extension UsersViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        guard let userCell = tableView.dequeueReusableCellWithIdentifier("UserTableViewCell", forIndexPath: indexPath) as? UserTableViewCell else {
            return cell
        }
        
        userCell.usernameLabel.text = dataSet[indexPath.row]
        
        return userCell
    }
    
}

extension UsersViewController: ConnectionManagerDelegate {
    
    func didUserConnectionUpdated(users: [String]) {
        dataSet = users
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.tableView.reloadData()
        }
    }
    
    func didStartVotingSession() { }
    
    func didStartVoting(votingName: String) {
        print("voting name \(votingName)")
    }
    
    func didFinishVoting(votingName: String) { }
    
    func didReciveVotes(votes: [String: AnyObject?]) { }
    
}
